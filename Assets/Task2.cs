﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2 : MonoBehaviour
{

    public int Exp = 100;
    public int Level = 10;
    void Update()
    {
        Exp++;
        if(Exp % 350 == 0)
        {
            Level++;
            Debug.Log("Достигнут уровень Level" + Level);
        }
    }
}
