﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task3 : MonoBehaviour
{
    public bool Check = true;
    public string Name = "Arno";

    private void Start()
    {
        Method();
    }
    public void Method()
    {
        if(Check == true && Name.Length < 4)
        {
            Debug.Log("Ошибка");
        }
        else if( Check == false)
        {
            Debug.Log("Вы должны согласиться с условиями использования сервиса");
        }
        else if (Check == true && Name.Length > 4)
        {
            Debug.Log("Ваше имя: " + Name);
        }
    }
}
