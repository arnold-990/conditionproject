﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomHouse : MonoBehaviour
{
    public string TargetCarColor, TargetCarType;
    public enum CarColor
    {
        золотой, 
        синий, 
        белый
    }
    public enum CarType
    {
        легковая, 
        внедорожник,  
        грузовик
    }
}
