﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task1 : MonoBehaviour
{
    public int Level = 10;
    public int FinishLevel = 15;
    void Start()
    {
        Method();
    }
    public  void Method()
    {
        if (Level < FinishLevel) 
        {
            Debug.Log("Continue");
        }
        else
        {
            Debug.Log("Game Over");
        }
    }
}
