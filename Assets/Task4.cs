﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task4 : MonoBehaviour
{
    public float Distance = 500;
    private void Update()
    {
        Method();
    }
    public void Method()
    {
        Debug.Log("Было" + Distance);
        if (Distance < 10 || Distance > 10) 
        {
            Distance *= 0.9f;
        }
        else
        {
            if(Distance > 0)
            {
                Distance += 1000;
            }
            else
            {
                Distance -= 1000;
            }
        }
        Debug.Log("Стало" + Distance);
    }
}
